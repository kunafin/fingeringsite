<?php

require_once 'vendor/autoload.php';
error_reporting(E_ALL|E_NOTICE);
// ---------------------------------------------------------------------
// --[ Main code ]------------------------------------------------------
// ---------------------------------------------------------------------


$url = 'https://www.kinopoisk.ru/film/915111/';

//$ip = '185.6.126.178:1080';
$ip = false;
$cookiefile = 'tmp/cookie.php';

$FingeringSite = new FingeringSite();

$FingeringSite->request($url, false, $cookiefile, false, $ip);

$html = $FingeringSite->html;

phpQuery::newDocument( $html );
phpQuery::newDocumentHTML($html, 'utf-8');

  $content['title'] = pq('h1')->text();
  $content['description'] = pq('[itemprop="description"]')->text();
  $content['rating'] = pq('[itemprop="ratingValue"]')->attr('content');
  $content['img'] = pq('.popupBigImage')->find('img')->attr('src');

  $actors = pq('#actorList>ul')->find('li');

  foreach ($actors as $el) {
    $pq = pq($el);

    $content['actors'][] = $pq->text();

  }
  $prop = pq('#infoTable table')->find('tr');
  foreach ($prop as $el) {
    $pq = pq($el);
    $td_name = trim($pq->find('td:nth-child(1)')->text());
    $td_value = trim($pq->find('td:nth-child(2)')->text());

    $content['props'][$td_name] = $td_value;

  }

phpQuery::unloadDocuments();

xd($content);
