<?php

class FingeringSite
{
	public $html;
	public $parentDom;
	public $chilrenDom;
	public $url;
	public $cookiefile;
	public $get_checker_proxy;
	public $get_selected_proxy;
	public $get_green_proxy;

	function __construct() {
		//die(__DIR__);
    }
    public function request($url, $postdata = null, $cookiefile = null, $sesion = true, $ip = null, $poxytype = null)
    {
		$ch = curl_init($url);
		$proxyauth = 'user:password';
		// установка URL и других необходимых параметров
		//curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36');
		curl_setopt( $ch, CURLOPT_HEADER, false );
		curl_setopt( $ch, CURLOPT_REFERER, "https://ya.ru" );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'X-Requested-With: XMLHttpRequest' ]);
		curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);

		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		if($cookiefile){
			curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookiefile );
			curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookiefile );
			curl_setopt( $ch, CURLOPT_COOKIESESSION, $sesion ); // хранит либо сбрасывает сессию, true не записывает сессионные куки, false - записывает
		}




		if($ip)
		{
			curl_setopt($ch, CURLOPT_PROXY, $ip);
		}
		if($this->IdentifiProxyType($poxytype))
		{
			curl_setopt($ch, CURLOPT_PROXYTYPE, $this->IdentifiProxyType($poxytype));
		}
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

		if( $postdata )
		{
			//curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $postdata );
		}


		// загрузка страницы и выдача её браузеру
		$html = curl_exec( $ch );

		// завершение сеанса и освобождение ресурсов
		if($html === false)
	    {
			$html = 'Curl error: ' . curl_error($ch);
	    }
		curl_close( $ch );
		$this->html = $html;

    }

	function multirequest($urls, $postdata = null, $cookiefile = null, $sesion = true, $ip = null, $poxytype = null)
	{
		$multi = curl_multi_init();
		$channels = array();
		$proxyauth = 'user:password';
		foreach ($urls as $url) {

		    $ch = curl_init($url);
		    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
			curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36');
			curl_setopt( $ch, CURLOPT_HEADER, false );
			curl_setopt( $ch, CURLOPT_REFERER, "https://ya.ru" );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'X-Requested-With: XMLHttpRequest' ]);
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);

			curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

			if($cookiefile)
			{
				//curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookiefile );
				//curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookiefile );
				curl_setopt( $ch, CURLOPT_COOKIESESSION, $sesion ); // хранит либо сбрасывает сессию, true не записывает сессионные куки, false - записывает
			}

			if($ip)
			{
				curl_setopt($ch, CURLOPT_PROXY, $ip);
			}
			if($this->IdentifiProxyType($poxytype))
			{
				curl_setopt($ch, CURLOPT_PROXYTYPE, $this->IdentifiProxyType($poxytype));
			}
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);


		    curl_multi_add_handle($multi, $ch);

		    $channels[$url] = $ch;
		}

		$active = null;
		do {
		    $mrc = curl_multi_exec($multi, $active);
		} while ($mrc == CURLM_CALL_MULTI_PERFORM);

		while ($active && $mrc == CURLM_OK) {
		    if (curl_multi_select($multi) == -1) {
		       	usleep(100);
		    }

		    do {
		        $mrc = curl_multi_exec($multi, $active);
		    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
		}

		foreach ($channels as $channel) {
		   $html = curl_multi_getcontent($channel) ;
		   if($html === false)
		    {
				$html = 'Curl error: ' . curl_error($ch);
		    }
		    $htmls[] = $html;
		    curl_multi_remove_handle($multi, $channel);
		}
		$this->html = $htmls;
		curl_multi_close($multi);
	}
    public function selected_proxy($max)
    {
		for( $start = 0; $start < $max; $start += 32 )
		{
			$url = 'http://hideme.ru/proxy-list/?maxtime=2000&type=s45&anon=4&start='.$start;
			$cookiefile = 'tmp/selected_proxy_cookie.php';

			$this->request($url, false, $cookiefile, false);

			phpQuery::newDocument( $this->html );

			$objIP = pq( ".proxy__t" )->find( 'tbody tr td:nth-child(1)' );
			$objPort = pq( ".proxy__t" )->find( 'tbody tr td:nth-child(2)' );
			$objType = pq( ".proxy__t" )->find( 'tbody tr td:nth-child(5)' );

			$arIP = $this->CreateArrayFoObj($objIP, array(), 'IP');
			$arIP = $this->CreateArrayFoObj($objPort, $arIP, 'PORT');
			$arIP = $this->CreateArrayFoObj($objType, $arIP, 'TYPE');

			foreach($arIP as $key=>$val) {
				if($val['TYPE'] == 'HTTP') {
					unset($arIP[$key]);
				} else {
					$arIPHiddenAss[$val['IP']] = $val;
				}
			}
			phpQuery::unloadDocuments();
		}
		if(empty($arIPHiddenAss)) {
			die('Хьюстон у нас проблемы, неудалось раздобыть проксю');
		}

		$srtIp = "";
		foreach ($arIPHiddenAss as $key => $value) {
			$srtIp .= $value['IP'].":".$value['PORT']."\n";
		}
		if($srtIp)
		{
			$this->get_selected_proxy = $srtIp;
			xprint('Добыли '.count($arIPHiddenAss).' проксей!');
			return $srtIp;
		}
    }
    public function checker_proxy($str_proxy)
    {
    	if(!$str_proxy) die('Хьюстон у нас проблема, не могу произвести анализ проксей!');

		$arProxy = explode("\n", $str_proxy);
		$arChunkProxy = array_chunk($arProxy, 100, TRUE);

		foreach ($arChunkProxy as $key => $value)
		{
			$str = implode("\n", $value);

			$url = 'http://www.checker.freeproxy.ru/engine/parser.php';


			$post['data'] = $str;

			$this->request($url, $post, false, false);

			//xprint($FingeringSite->html);

			$post = $this->html;

			sleep(20);
			$url = "http://www.checker.freeproxy.ru/engine/results.php";
			$this->request($url, false, $cookiefile, false);


			$data = json_decode($this->html);

			if($data->worked > 0)
			{
				foreach ($data->list as $key => $value)
				{
					if($value->status == "valid")
					{
						$type_proxy = "";
						if(!in_array($value->http, array("FAIL", "null"))) {
							$type_proxy = "HTTP";
						} elseif(!in_array($value->https, array("FAIL", "null"))) {
							$type_proxy = "HTTPS";
						} elseif(!in_array($value->socks4, array("FAIL", "null"))) {
							$type_proxy = "SOCKS4";
						} elseif(!in_array($value->socks5, array("FAIL", "null"))) {
							$type_proxy = "SOCKS5";
						} else {
							continue;
						}

						$arValidProxy[$value->ip] = [
							"ip" => $value->ip,
							"port" => $value->port,
							"type_proxy" => $type_proxy
						];
					}
				}

			}
		}

		$srtIp = json_encode($arValidProxy);

		if($arValidProxy)
		{
			xprint('Первый тест показал, что '.count($arValidProxy).' проксей готовы к работе');

			$this->get_selected_proxy = $srtIp;
			return $srtIp;
		}
    }
    public function green_proxy($url, $str_proxy, $cookiefile)
    {
    	//$str_proxy = file_get_contents("tmp/checker_proxy.txt");
    	if(!$url) die('Выбери тестовый урл');
    	if(!$str_proxy) die('Список прокси пуст');
		$arProxy = json_decode($str_proxy);
		$i = 0;
		foreach ($arProxy as $key => $poxy) {

			$ip = $poxy->ip.":".$poxy->port;
			$type_proxy = $poxy->type_proxy;

			$this->request($url, false, $cookiefile, false, $ip, $type_proxy);

			xprint($this->html);
			if(strpos($this->html, 'Curl error') === false) {
				$i++;
				$arGreenProxy[] = $poxy;

				if($i>5) break;

			}
		}

		if($arGreenProxy)
		{
			$srtIp = json_encode($arGreenProxy);

			xprint(count($arGreenProxy).' готовых к работе проксей');
			$this->get_green_proxy = $srtIp;
			return $srtIp;
		}

    }

	public function pareseId($bx_id)
	{
	    $id = explode('_', $bx_id);

	    return array_pop($id);
	}
	public function getIDs()
	{
		phpQuery::newDocument( $this->html );

		$yp_sektor = pq( $this->parentDom )->find( $this->chilrenDom );

		foreach($yp_sektor as $product)
		{
			$product = pq($product);

			$bx_id = $product->attr('id');
			$arID[] = $this->pareseId($bx_id);
		}

		phpQuery::unloadDocuments();
		return $arID ;
	}
	public function getBasket()
	{
		phpQuery::newDocument( $this->html );

		$html = pq( '#bx_cart_block1' )->html();

		phpQuery::unloadDocuments();

		return $html ;
	}
	private function IdentifiProxyType($poxytype)
	{
		$curlpoxytype = "";
	    switch ($poxytype) {
	        case "SOCKS5":
	            $curlpoxytype = CURLPROXY_SOCKS5;
	            break;
	        case "SOCKS4":
	             $curlpoxytype = CURLPROXY_SOCKS4;
	            break;
	        case "HTTP":
	             $curlpoxytype = CURLPROXY_HTTP;
	            break;
	    }
	    return $curlpoxytype;
	}
	private function CreateArrayFoObj($obj, $array = array(), $index)
	{
	    $i = 0;
	    foreach($obj as $val)
	    {
	        $array[$i++][$index] = pq($val)->text();
	    }
	    return $array;
	}
}