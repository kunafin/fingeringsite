<?php
require_once 'vendor/autoload.php';
error_reporting(E_ALL|E_NOTICE);
// ---------------------------------------------------------------------
// --[ Main code ]------------------------------------------------------
// ---------------------------------------------------------------------
$str_proxy = file_get_contents("tmp/selected_proxy.txt");
if(!$str_proxy) die('Файл с прокси пуст');

$arProxy = explode("\n", $str_proxy);
$arChunkProxy = array_chunk($arProxy, 100, TRUE);

foreach ($arChunkProxy as $key => $value)
{
	$str = implode("\n", $value);

	$url = 'http://www.checker.freeproxy.ru/engine/parser.php';
	$cookiefile = 'tmp/checker_cookie.php';

	$FingeringSite = new FingeringSite();
	$post['data'] = $str;

	$FingeringSite->request($url, $post, $cookiefile, false);

	//xprint($FingeringSite->html);

	$post = $FingeringSite->html;

	sleep(20);
	$url = "http://www.checker.freeproxy.ru/engine/results.php";
	$FingeringSite->request($url, false, $cookiefile, false);


	$data = json_decode($FingeringSite->html);

	if($data->worked > 0)
	{
		foreach ($data->list as $key => $value)
		{
			if($value->status == "valid")
			{
				$type_proxy = "";
				if(!in_array($value->http, array("FAIL", "null"))) {
					$type_proxy = "HTTP";
				} elseif(!in_array($value->https, array("FAIL", "null"))) {
					$type_proxy = "HTTPS";
				} elseif(!in_array($value->socks4, array("FAIL", "null"))) {
					$type_proxy = "SOCKS4";
				} elseif(!in_array($value->socks5, array("FAIL", "null"))) {
					$type_proxy = "SOCKS5";
				} else {
					continue;
				}

				$arValidProxy[$value->ip] = [
					"ip" => $value->ip,
					"port" => $value->port,
					"type_proxy" => $type_proxy
				];
			}
		}

	}
}

$srtIp = json_encode($arValidProxy);

if($arValidProxy)
{
	xprint('write '.count($arValidProxy).' checker_proxy');
	file_put_contents("tmp/checker_proxy.txt", $srtIp);
}





